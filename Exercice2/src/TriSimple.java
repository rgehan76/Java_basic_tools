/*
 * ENSICAEN
 *  6 Boulevard Maréchal Juin
 *  F-14050 Caen Cedex
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 * @author Robin Gehan <rgehan@ecole.ensicaen.fr>
 * @version     0.0.1 - 2019-21-03
 * @todo nothing
 *  @bug nothing
 */

import java.util.Arrays;
/**
 * A Class when i can sort Integers into Array
 *
 * @author Robin Gehan
 * @version 1.0
 */
public class TriSimple {

    private int [] tab ;
    private int increment;
    private int iter;

    /**
     * Default constructor , Array-size : 10, increment:5
     */
    public TriSimple()
    {
        this.tab = new int [10];
        this.increment=5;
    }

    /**
     *
     * @param taille size of the array
     * @param increment value of the increment
     */
    public TriSimple( int taille , int increment)
    {
        this.tab = new int [taille];
        this.increment=increment;
    }


    /**
     *
     * @param entier Integer you want to insert
     */
    public void inserer (int entier)
    {
        if(this.iter<this.tab.length)
        {
            this.tab[this.iter++] = entier;

        }
        else
        {
            this.incrementer();
            this.inserer(entier);
        }

        Arrays.sort(this.tab,0,this.iter);
    }


    /**
     *
     * @param entier Integer you want to delete, onyl first will be delete
     */
    public void supprimer (int entier)
    {
        for(int cpt=0;cpt<this.iter;cpt++)
        {
            if(entier==this.tab[cpt])
            {
                this.tab[cpt]=6500;

                Arrays.sort(this.tab,0,this.iter--);
                this.tab[this.iter]=0;

            }

        }

    }


    /**
     * private method used to increase the size of the Array
     */
    private void incrementer ()
    {
        this.tab=Arrays.copyOf(this.tab,this.tab.length+this.increment);
    }


    /**
     *
     * @return Representation of the Array
     */
    public String toString()
    {
        String s="";

        for(int cpt=0; cpt<this.tab.length;cpt++)
        {
            s+=" "+this.tab[cpt]+" |";

        }

        return s;

    }

}
