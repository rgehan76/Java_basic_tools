/*
 * ENSICAEN
 *  6 Boulevard Maréchal Juin
 *  F-14050 Caen Cedex
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 * @author Robin Gehan <rgehan@ecole.ensicaen.fr>
 * @version     0.0.1 - 2019-21-03
 * @todo nothing
 *  @bug nothing
 */
/**
 * My main class where i can test my programm
 *
 * @author Robin Gehan
 * @version 1.0
 */
public class Main {

    public static void main(String[] args) {

        TriSimple ts = new TriSimple(10,5);

        /* J'insère 10 valeurs puis j'affiche */

        ts.inserer(10);
        ts.inserer(11);
        ts.inserer(12);
        ts.inserer(13);
        ts.inserer(5);
        ts.inserer(7);
        ts.inserer(4);
        ts.inserer(5);
        ts.inserer(6);
        ts.inserer(8);
        System.out.println(ts);




        /* J'insère une onzième valeur puis j'affiche
            pour vérifier que mon tableau a bien changé de taille
         */
        ts.inserer(7);
        System.out.println(ts);





        /* Je supprime le 8 et le 10 puis j'affiche à nouveau en
            vérifiant qu'ils ne sont plus dans le tableau
         */

        ts.supprimer(8);
        ts.supprimer(10);

        System.out.println(ts);

    }


}
