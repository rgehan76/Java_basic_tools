/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 * @author Robin Gehan <rgehan@ecole.ensicaen.fr>
 * @version     0.0.1 - 2019-03-22
 * @todo nothing
 * @bug nothing
 *
 */

/**
 * A public class to represents something ordenable
 *
 * @author Robin Gehan
 * @version 1.0
 */
public class EntiersOrdonnables implements Ordonnable, Comparable<Object>{

    private int entier;


    /**
     *
     * @param chaine string you want to compare
     */
    public EntiersOrdonnables(String chaine)
    {
        this.entier = Integer.parseInt(chaine);
    }

    /**
     * @param o object you want to compare with
     * @return true if objects are equivalent, false either
     */
    public boolean egal(Object o)
    {
        return o instanceof  Ordonnable;
    }

    /**
     *
     * @return value in integer
     */
    public int getChaineEnEntier()
    {
        return this.entier;
    }

    /**
     *
     * @param o object you want to compare with
     * @return 0 if objects got the same value, 1 if the current instance is heavier than the one in parameter, -1 either
     */
    @Override
    public int compareTo(Object o)
    {
        if(! this.egal(o))
        {
            new Exception("Erreur ");
        }

        if(this.entier ==  ((EntiersOrdonnables)o).getChaineEnEntier())
        {
            return 0;
        }

        if(this.entier > ((EntiersOrdonnables)o).getChaineEnEntier())
        {
            return 1;
        }

        return -1;
    }

     /**
     *
     * @return all informations about the current instance
     */
    public String toString()
    {
        return String.valueOf(this.entier);
    }

}
