/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 * @author Robin Gehan <rgehan@ecole.ensicaen.fr>
 * @version     0.0.1 - 2019-03-22
 * @todo nothing
 * @bug nothing
 *
 */

/**
 * A public class to test my programm
 *
 * @author Robin Gehan
 * @version 1.0
 */
public class TestEntiersOrdonnables {

    public static void main(String args[])
    {

	/* Test d'insertions et de incrémentations */
        TableauTrie tableauTrie = new TableauTrie(5,2);
        tableauTrie.inserer(new EntiersOrdonnables("56"));
        tableauTrie.inserer(new EntiersOrdonnables("12"));
        tableauTrie.inserer(new EntiersOrdonnables("65"));
        tableauTrie.inserer(new EntiersOrdonnables("18"));
        tableauTrie.inserer(new EntiersOrdonnables("20"));

        System.out.println(tableauTrie.toString());

        tableauTrie.inserer(new EntiersOrdonnables("1"));
        tableauTrie.inserer(new EntiersOrdonnables("3"));
        tableauTrie.inserer(new EntiersOrdonnables("5"));
        tableauTrie.inserer(new EntiersOrdonnables("7"));

        System.out.println(tableauTrie.toString());


	/* Test de suppression */

        tableauTrie.supprimer(new EntiersOrdonnables("12"));
        tableauTrie.supprimer(new EntiersOrdonnables("7"));
  
        System.out.println(tableauTrie.toString());

    }
}
