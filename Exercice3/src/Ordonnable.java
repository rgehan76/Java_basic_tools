/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 * @author Robin Gehan <rgehan@ecole.ensicaen.fr>
 * @version     0.0.1 - 2019-03-22
 * @todo nothing
 * @bug nothing
 *
 */

/**
 * A public interface to compare instance of Objects
 *
 * @author Robin Gehan
 * @version 1.0
 */
public interface Ordonnable {
    /**
     * @param o object you want to compare with
     * @return true if objects are equivalent, false either
     */
    public boolean egal(Object o);
}
