/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 * @author Robin Gehan <rgehan@ecole.ensicaen.fr>
 * @version     0.0.1 - 2019-03-22
 * @todo nothing
 * @bug nothing
 *
 */


import java.util.Arrays;

/**
 * A public class to represents an Array of EntiersOrdonnables
 *
 * @author Robin Gehan
 * @version 1.0
 */
public class TableauTrie {

    private Ordonnable[] tab;
    private int          increment;
    private int          indiceEltInsere;

    /**
     * @param tailleTab original size of the array
     * @param increment size of the increment
     */
    public TableauTrie(int tailleTab, int increment)
    {
        this.tab             = new Ordonnable[tailleTab];
        this.increment       = increment;
        this.indiceEltInsere = 0;
    }

    /**
     * fonction used to insert an Object in the Array
     * @param ordonnableInstance the Object you want to add
     */
    public void inserer(Ordonnable ordonnableInstance)
    {
        if(indiceEltInsere < tab.length)
        {
            this.tab[this.indiceEltInsere++] = ordonnableInstance;
            Arrays.sort(tab, 0, indiceEltInsere);
        }
        else
        {
            this.incrementer (ordonnableInstance);
        }
    }

    /**
     * fonction used to increase the size oh the array
     * @param ordonnableInstance the Object you want to add
     */
    private void incrementer (Ordonnable ordonnableInstance)
    {
        this.tab = Arrays.copyOf(tab, (tab.length + increment));
        tab[indiceEltInsere++] = ordonnableInstance;
        Arrays.sort(tab, 0, indiceEltInsere);
    }
    
    /**
     * @param ordonnableInstance The Object you want to delete
     */
    public void supprimer(Ordonnable ordonnableInstance)
    {

            for(int cpt=0;cpt<this.indiceEltInsere;cpt++)
            {
                /* Cast des objets */
                EntiersOrdonnables e = (EntiersOrdonnables)ordonnableInstance;
                EntiersOrdonnables e2 = (EntiersOrdonnables)tab[cpt];
                if(e2.compareTo(e)==0)
                {
                    this.tab[cpt]= null;
                    Ordonnable temp;
                    for(int cpt2 = cpt; cpt2<this.indiceEltInsere-1;cpt2++)
                    {
                        temp=this.tab[cpt2];
                        this.tab[cpt2]=this.tab[cpt2+1];
                        this.tab[cpt2+1]=temp;
                    }
                    this.indiceEltInsere--;
                    break;
                }
        }
    }

    /**
     * @return all informations about the current Array
     */
    public String toString()
    {
        String res = "Le tableau suivant est de taille "+tab.length +" \n" + "Il contient l'ensemble des " +
                "elements suivants:\n";

        for(int i = 0; i< indiceEltInsere; i++)
        {
            res += tab[i].toString() + " ";
        }
        return res + "\n";
    }

}
