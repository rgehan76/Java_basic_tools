/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 * @author Robin Gehan <rgehan@ecole.ensicaen.fr>
 * @version     0.0.1 - 2018-10-15
 * @todo nothing
 * @bug nothing
 *
 */
/**
 * A Manuel is Book with more attributs
 *
 * @author Robin Gehan
 * @version 1.0
 */
public class Manuel extends Livre {
    private int niveau;

    /**
     *
     * @param numero number of the Manual
     * @param nom manual name
     * @param auteur author name
     * @param nbPages number of pages
     * @param niveau level of the manual
     */
    public Manuel(int numero,String nom,String auteur, int nbPages,int niveau)
    {
        super(numero,nom,auteur,nbPages);
        this.niveau=niveau;
    }

}
