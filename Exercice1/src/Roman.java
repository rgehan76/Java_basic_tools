/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 * @author Robin Gehan <rgehan@ecole.ensicaen.fr>
 * @version     0.0.1 - 2018-10-15
 * @todo nothing
 * @bug nothing
 *
 */

/**
 * A Roman is Book with more attributs
 *
 * @author Robin Gehan
 * @version 1.0
 */
public class Roman extends Livre {
    private final String [] tab = {"GONCOURT", "MEDICIT","INTERALLIE"};
    private int prixL;

    /**
     *
     * @param numero number of The roman
     * @param nom title
     * @param auteur author name
     * @param nbPages number of pages
     * @param prixL 0 if Goncourt 1 if MEDICIT 2 if INTERALLIE
     */
    public Roman (int numero,String nom,String auteur, int nbPages,int prixL)
    {
        super(numero,nom,auteur,nbPages);
        this.prixL=prixL;
    }

}
