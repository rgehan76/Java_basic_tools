/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 * @author Robin Gehan <rgehan@ecole.ensicaen.fr>
 * @version     0.0.1 - 2018-10-15
 * @todo nothing
 * @bug nothing
 *
 */
import java.util.ArrayList;



/**
 * A Library with some Document
 *
 * @author Robin Gehan
 * @version 1.0
 */
public class Bibliotheque {

    protected int livresMax;
    protected Livre [] livres;
    protected int enCours;


    /**
     * @param livreMax max number of Document in the Library
     */
    public Bibliotheque(int livreMax) {
        this.livresMax = livreMax;
        this.livres = new Livre[livreMax];
    }


    public int getCapacite()
    {
        return this.livresMax;
    }

    public int getLivresMax() {
        return livresMax;
    }

    /**
     * @param livresMax new max number of Document in the Library
     */
    public void setLivresMax(int livresMax) {
        this.livresMax = livresMax;
    }

    /**
     * @return All Document of the Library
     */
    public Document[] getLivres() {
        return livres;
    }

    /**
     * @param livres Array of new Document
     */
    public void setLivres(Livre[] livres) {
        this.livres = livres;
    }





    /**
     *
     * @param d the document you wants to add
     * @throws BibiliothequeException if the librabry is full of books
     */
    public void ajoutDocument(Livre d) throws BibiliothequeException
    {
        if(this.enCours<this.livresMax)
        {
            this.livres[this.enCours++]=d;
        }

        else {
            throw new BibiliothequeException(d.getTitre() + " n'a pas pu être ajouté");
        }
    }


    /**
     *
     * @param nomAuteur name of the author wich you want to find Books
     * @return Array of the Books
     */


    public ArrayList<Livre> getLivresByName (String nomAuteur)
    {
        ArrayList<Livre> livresRet = new ArrayList<Livre>();

        for(int cpt=0; cpt<this.enCours;cpt++)
        {

            if(this.livres[cpt].getAuteur().equals(nomAuteur))
                livresRet.add(this.livres[cpt]);

        }


        return livresRet;

    }

    /**
     * @return position of the current Document in the Library
     */
    public int getNbDocuments()
    {
        return this.enCours;
    }

    /**
     * @return all informations about the Library
     */
    public String toString ()
    {
        String s="";

        for (int cpt=0; cpt<this.enCours;cpt++)
        {
            s+=this.livres[cpt].toString();
            s+="\n";

        }

        return s;
    }

}
