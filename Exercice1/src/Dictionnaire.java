/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 * @author Robin Gehan <rgehan@ecole.ensicaen.fr>
 * @version     0.0.1 - 2018-10-15
 * @todo nothing
 * @bug nothing
 *
 */

/**
 * A Dictionnary is Document with more attributs
 *
 * @author Robin Gehan
 * @version 1.0
 */
public class Dictionnaire extends Document {

    private final String [] tab = {"ANGLAIS", "ALLEMAND","ESPANGOL"};
    private int langue;

    /**
     *
     * @param numero number of the Dictionnary
     * @param nom title
     * @param langue 0 for ANGLAIS , 1 for ALLEMAND, 2 for ESPANGOL
     */
    public Dictionnaire(int numero,String nom, int langue)
    {
        super(numero,nom);
        this.langue=langue;
    }
}
