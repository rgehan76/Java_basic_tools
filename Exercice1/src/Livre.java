/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 * @author Robin Gehan <rgehan@ecole.ensicaen.fr>
 * @version     0.0.1 - 2018-10-15
 * @todo nothing
 * @bug nothing
 *
 */
/**
 * A Book is Document with more attributs
 *
 * @author Robin Gehan
 * @version 1.0
 */
public class Livre extends Document {
    private String auteur;
    private int nbPages;

    /**
     *
     * @param numero nmuber of the Book
     * @param nom name of the Book
     * @param auteur author of the book
     * @param nbPages number of pages
     */
    public Livre(int numero, String nom, String auteur, int nbPages)
    {
        super(numero,nom);
        this.auteur=auteur;
        this.nbPages=nbPages;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    /**
     *
     * @return All informations about a Book
     */
    @Override
    public String toString() {
        return super.toString()+",auteur : "+this.auteur + ", nbPages :"+this.nbPages;
    }
}
