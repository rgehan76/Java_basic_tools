/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 * @author Robin Gehan <rgehan@ecole.ensicaen.fr>
 * @version     0.0.1 - 2019-01-03
 * @todo nothing
 * @bug nothing
 *
 */

/**
 * A docuement with a number and and a title
 *
 * @author Robin Gehan
 * @version 1.0
 */
public class Document {

    private int numero;
    private String titre;

    public Document(int numero, String titre) {
        this.numero = numero;
        this.titre = titre;
    }

    public Document(Document d) {
        this(d.getNumero(), d.getTitre());
    }


    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }


    /**
     *
     * @param o The object you want to compare with
     * @return true if the number is the same, false either
     */
    public boolean equals(Object o) {

        if (o.getClass() == this.getClass()) {

            Document d = (Document) o;
            return (this.numero == d.getNumero());
        }

        return false;
    }


    public String toString() {
        return "numéro : " + this.numero + " ,titre :" + this.titre;

    }

}
