/*
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 * @author Robin Gehan <rgehan@ecole.ensicaen.fr>
 * @version     0.0.1 - 2018-10-15
 * @todo nothing
 * @bug nothing
 *
 */

import java.util.ArrayList;

/**
 * An Exception class for Bibliotheque
 *
 * @author Robin Gehan
 * @version 1.0
 */
public class BibiliothequeException extends Exception {


    public BibiliothequeException (String max)
    {
        super(max);
    }


}
