import sun.awt.geom.AreaOp;

import java.util.ArrayList;

/**
 * My main class where i can test my programm
 *
 * @author Robin Gehan
 * @version 1.0
 */
public class Main {
    public static void main(String[] args)  {

        /* Test de la classe Document */
        Document d = new Document(10," Charlotte aux fraises");

        System.out.println(d);
        System.out.println(d.equals("oui"));




        /* Test de la méthode getLivresByName */
        Bibliotheque b = new Bibliotheque(5);


        Livre l = new Livre (70,"La petite madeleine","Proust",50);
        Livre l2 = new Livre (71,"ouioui","PasProust",50);
        Livre l3 = new Livre (72,"EncoreLui","Proust",50);
        try {
            b.ajoutDocument(l);
            b.ajoutDocument(l2);
            b.ajoutDocument(l3);
        }

        catch (BibiliothequeException bb)
        {
            System.out.println(bb);
        }
        System.out.println("Affichage de la Bibliothèque :");
        System.out.println(b);
        ArrayList<Livre> proust = new ArrayList<Livre>();

        proust = b.getLivresByName("Proust");

        System.out.println(proust);




        /* Test de l'excption */


        b = new Bibliotheque(5);

        try
        {
            b.ajoutDocument(l);
            b.ajoutDocument(l2);
            b.ajoutDocument(l3);
            b.ajoutDocument(new Livre(5,"no idea","Proust",72));
            b.ajoutDocument(new Livre(70,"je sais pas","Proust",73));

        }

        catch (BibiliothequeException bb)
        {
            System.out.println(bb);
        }

        System.out.println("JAFFICHE MA BIBLIOTHEQUE");
        System.out.println(b);


        try
        {
            Thread.sleep(1000);
        }

        catch (InterruptedException i)
        {
            System.out.println(i);
        }
        System.out.println("JE METS UN LIVRE EN TROP POUR VOIR ");


        try
        {
            b.ajoutDocument(new Livre(10,"j'adore les prunes","Proust",88));
        }

        catch(BibiliothequeException bb)
        {
            System.out.println(bb);
        }

        System.out.println("JE REGARDE SI LES PRUNES SE SONT AJOUTEES");
        System.out.println(b);
        System.out.println();








    }
}
